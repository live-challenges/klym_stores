# Klym Stores

Klym Stores is opening retail stores across Chile, Colombia, Brazil and USA. It was decided that all stores are going to provide the same catalog of products regardless of the country. Furthermore, the price of the products in dollars (USD) is going to be the same across countries. However, stores in each country should be able to query the products price in the country's currency (CLP, COP, BRL or USD) so they can sell to their local customers. You are asked to provide an API that allows users to: 

- Register new products to the catalog of products with their price in USD. 
- Update a given product price in USD. 
- Query the products with their price in the local currency. While today we only support 4 currencies, in the future Klym Stores may expand to more countries with new currencies. 

Note that the communication from the stores (from which these endpoints are going to be consumed) and the backend is not reliable, so the services must be prepared to handle multiple retries. 

## Currencies conversion

In order to do the currencies conversions, an external service which provides the real time conversion rates from USD to other currencies was contracted. This service is provided in the `docker-compose.yml` file and can be run with:

```shell
make start-currencies-server
```

This service offers the following endpoint:

### Request

```
GET /USD/to/{desired_currency} 
```
  
Where `desired_currency`  must be one of:
 - `USD`
 - `COP`
 - `CLP`
 - `BRL`

### Response
 
```json
{
  "rate": 4200.15
} 
```

Where `rate` is the conversion rate from USD to `desired_currency`. 

## Requirements

Implement a service providing the required functionality. Note that, while you are going to be designing and implementing this service, it is going to be maintained long term by a different team. This means that, besides working (and maintainable) code, you are expected to provide all the documentation, tools and configurations required to do a successful handover of the project and to enable this team to maintain the project long term. 

Furthermore, while the current implementation is going to be done using HTTP APIs, there are discussions to migrate these services to gRPC or similar technologies in the following years. The code should be structured in such a way that this kind of migration can be done as painlessly as possible if the migration is approved.  

The deliverable is a MR to this repository with the implemented service, documentation and tools.  

### Restrictions: 

- The service must be implemented using the main language used by your team (python or java). But you are free to pick your preferred framework.
- The service must provide the desired functionality through a REST HTTP API.
- The estimated time to complete the exercise is 2 hours. You are expected to time box your development time. It's ok to deliver partial results.

### Out of scope: 

- Authentication/authorization is out of scope. 
- Deleting/invalidating products is out of scope. 
- Sales, inventory management, etc., are out of scope. 